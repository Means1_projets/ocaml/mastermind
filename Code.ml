(** Module de definition d'un code dans le jeu Mastermind **)
open Str
module Code:
	sig
		(** Le type d'un pion *)
		type pion = Pion of string * int * (int * int * int)
		
		
		(** Le type d'un code *)
		type t = pion list
		
		
		(** Nombre de pions par code *)
		val nombre_pions : int
		
		
		(** Liste des couleurs possibles *)
		val couleurs_possibles : pion list
		
		
		
		(** Compare deux codes
		*	@param code1 premier code a comparer
		*	@param code2 second  code a comparer
		*	@return 0 si les deux codes sont identiques,
			un entier positif si [code1] est strictement plus grand que [code2]
			un entier negatif si [code1] est strictement plus petit que [code2]
		*)
		val compare : t -> t -> int
		
		
		
		(** Conversion code vers chaine de caracteres (pour affichage)
		*	@param code code a convertir
		*	@return la representation en chaine de caracteres de [code]
		*)	
		val string_of_code : t -> string
		
	
		(** Conversion chaine de caracteres vers code (pour saisie)
		*	@param string chaine de caractere saisie
		*	@return le code correspondant a la saisie si la conversion est possible
			[None] si la conversion n'est pas possible
		*)	
		val code_of_string : string -> t option
		
		(** La liste de tous les codes permis (par défault de longueur 4) *)
		val tous : t list ref


		(** Update la liste de tous les codes permis *)
		val tous_update : int -> unit
		
		
		(** La liste de toutes les reponses possibles *)
		val toutes_reponses : ( int * int ) list ref;;
		
		(** Update la liste de toutes les reponses possibles *)
		val toutes_reponses_update : int -> unit;;
		
		
		(** Calcule la reponse d'un code par rapport au code cache
		*	@param      code le code propose
		*	@param vrai_code le code cache
		*	@return un couple (nombre de pions bien places, 
				nombre de pions mal places)
				[None] si la reponse ne peut etre calculee
		*)
		val reponse : t -> t -> ( int * int ) option

	end =
	struct
		type pion = Pion of string * int * (int * int * int);;
		type t = pion list
		let nombre_pions = 4;;
		let couleurs_possibles = [	Pion ("Rouge", 0, (255, 14, 19)); Pion ("Vert", 1, (51, 254, 55)); Pion ("Jaune", 2, (255, 255, 30)); Pion ("Bleu", 3, (0, 83, 210)); Pion ("Blanc", 4, (255, 255, 255)); Pion ("Noire", 5, (0, 0, 0))];;
		let rec compare a b = match (a, b) with
											((Pion(_,c,_) :: r1), (Pion(_,d,_) :: r2)) -> let e = c- d in if e == 0 then compare r1 r2 else e
											| ([], []) -> 0
											| _ -> failwith "Error length of list";;
		let rec string_of_code a= match a with
											| (Pion(c,_,_)::r) -> c ^ " "  ^ string_of_code r
											| [] -> "" ;;
		
		let tous_fct i = 
			let rec combi n a =
			  if n = 0 then [[]]
			  else
				let res = combi (n-1) a in
				List.concat (List.map (fun m -> List.map(fun s -> s@m) a) res) in combi i (List.map (fun x -> [x]) couleurs_possibles);;

		let tous = ref (tous_fct nombre_pions);;										
		
		let tous_update i = tous := (tous_fct i);;										
		

		let code_of_string a = try Some(List.map (fun x -> (List.find (fun (Pion(name,_,_)) -> name = x) couleurs_possibles)) (Str.split (Str.regexp " ") a)) with _ -> None;;
		


		let toutes_reponses_fct nombre_pions = let rec  fct1 a b = match (a,b) with
																(a, b) when b > nombre_pions-2 -> [(b, 0)]
																| (-1,_) -> []
																| (a,_) -> (b, a) :: (fct1 (a-1	) b) and fct2 a = match a with
																a when a < 0 -> []
																| a -> (fct1 (nombre_pions - a) (a))  @ (fct2 (a-1)) in fct2 nombre_pions;;
		

		let toutes_reponses = ref (toutes_reponses_fct nombre_pions);;


		let toutes_reponses_update i = toutes_reponses := (toutes_reponses_fct i);;



		let reponse a b = let rec fct a b acc = (match (a,b) with
															((e1 :: r1), (e2 :: r2)) -> if e1=e2 then acc :: (fct r1 r2 (acc+1)) else fct r1 r2 (acc+1)
															| ([], []) -> []
															| _ -> failwith "length") and fct2 a b acc = (match a with
																										(e :: r) -> if ((List.exists (fun x -> x = acc) b)) then (fct2 r b (acc + 1)) else e :: (fct2 r b (acc + 1))
																										| [] -> []) 
																					and fct3 a = (match a with 
																								(e :: r) -> (e, List.fold_left (fun x y -> if (y = e) then x + 1 else x) 1 r) :: (fct3 (List.filter (fun x -> e <> x) r))
																								| [] -> []) in 
							try let c = fct a b 0 in let d = fct3 (fct2 a c 0) and e = fct3 (fct2 b c 0) in 
								Some (List.length c, List.fold_left (fun acc1 (x1, x2) -> List.fold_left (fun acc2 (x3, x4) -> if x1 = x3 then (if x2 > x4 then x4 + acc2 else x2 + acc2) else acc2) acc1 e) 0 d) with _ -> None;;
	end;;
