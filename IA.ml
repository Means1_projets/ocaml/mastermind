open Code


(** Algorithmes de recherche de code *) 
module IA : 
	sig 
		(** Nombre d'algorithmes developpes *) 
		val nombre_methodes : int
		
		
		(** Choisit un code a proposer
		* 	@param methode 0 pour l'algorithme naif, 
		*		1 pour l'algorithme de KNUTH 
		*		... et ainsi de suite 
		*	@param essais la liste des codes deja proposes 
		*	@param possibles la liste des codes possibles 
		*	@return le prochain code a essayer 
		*) 
		val choix : int -> Code.t list -> Code.t list -> int -> Code.t
		
		
		(** Filtre les codes possibles 
		*	@param methode 0 pour l'algorithme naif, 
		*		1 pour l'algorithme de KNUTH 
		* 		... et ainsi de suite 
		*	@param (code, rep) le code essaye et la reponse correspondante 
		*	@param possibles la liste de courante de codes possibles 
		*	@return la nouvelle liste de codes possibles 
		*) 
		val filtre : int -> (Code.t * (int * int) option) -> Code.t list -> Code.t list 
	
	end =
	struct
		let nombre_methodes = 4;;


		let rec generateCode acc tailleCode = match acc with
			| acc when ((tailleCode / 2) > acc) -> (List.nth Code.couleurs_possibles 0) :: (generateCode (acc + 1) tailleCode)
			| acc when acc < tailleCode -> (List.nth Code.couleurs_possibles 1) :: (generateCode (acc + 1) tailleCode)
			| _ -> [];;


		let choix algo lastTry possibleCode tailleCode = match algo with
		| 0 -> List.nth possibleCode (Random.int (List.length possibleCode))
		| 1 -> (match lastTry with
			| [] -> List.nth possibleCode (Random.int (List.length possibleCode))
			| _ -> List.nth possibleCode 0)
		| 2 -> (match lastTry with
			| [] -> (generateCode 0 tailleCode)
			| _ -> begin
					let countP = ref 0 in
					let sizeofT = List.length (!Code.tous) in
					let sizeofS = List.length possibleCode in
					let (codes, _) = List.fold_left (fun (codes, score) y -> countP := (!countP) + 1;
																				print_endline (string_of_float (((float_of_int (!countP)) /. (float_of_int sizeofT)) *. 100.));
					(match (
						List.fold_left (fun hit (b, w) -> 
							let value = (List.fold_left (fun x possCode -> let result2 = Code.reponse y possCode in (match result2 with
									| Some(b2, w2) when b2 <> b || w <> w2 -> x+1
									| _ -> x)) 0 possibleCode) in 
								if (hit < value) then value else hit) sizeofS (!Code.toutes_reponses)
					) with
					| newScore when score < newScore -> ([y], newScore)
					| newScore when score = newScore -> (y :: codes, score)
					| _ -> (codes, score))) ([[]], 0) (!Code.tous) in 
					try List.find (fun x -> List.exists (fun y -> (Code.compare x y) = 0) possibleCode) codes with _ -> List.nth codes 0
					
					end)
		| 3 -> (match lastTry with
			| [] -> (generateCode 0 tailleCode)
			| _ -> begin
					let countP = ref 0 in
					let sizeofT = List.length (!Code.tous) in
					let sizeofS = List.length possibleCode in
					let (codes, _) = List.fold_left (fun (codes, score) y -> countP := (!countP) + 1;
																				print_endline (string_of_float (((float_of_int (!countP)) /. (float_of_int sizeofT)) *. 100.));
					(match (
						List.fold_left (fun hit (b, w) -> 
							let value = (List.fold_left (fun x possCode -> let result2 = Code.reponse y possCode in (match result2 with
									| Some(b2, w2) when b2 <> b || w <> w2 -> x+1
									| _ -> x)) 0 possibleCode) in 
								if (hit > value) then value else hit) sizeofS (!Code.toutes_reponses)
					) with
					| newScore when score < newScore -> ([y], newScore)
					| newScore when score = newScore -> (y :: codes, score)
					| _ -> (codes, score))) ([[]], 0) (!Code.tous) in 
					try List.find (fun x -> List.exists (fun y -> (Code.compare x y) = 0) possibleCode) codes with _ -> List.nth codes 0
					
					end)
		| _ -> [];;

		let filtre algo codeAndResult listPossible = match algo with
		| 0 -> (
				match codeAndResult with
				| (code, Some (b, w)) -> List.filter (fun a -> (Code.compare code a) <> 0) listPossible
				| _ -> (listPossible))
		| 1 -> (match codeAndResult with
			| (code, Some (b, w)) -> List.filter (fun x -> let result2 = Code.reponse code x in (match  result2 with
																									Some (b2, w2) -> b2 = b && w2 = w
																									| None -> false)) listPossible
			| _ -> listPossible)
		| 2 -> (match codeAndResult with
			| (code, Some (b, w)) -> List.filter (fun x -> let result2 = Code.reponse code x in (match  result2 with
																									Some (b2, w2) -> b2 = b && w2 = w
																									| None -> false)) listPossible
			| _ -> listPossible)
		| 3 -> (match codeAndResult with
			| (code, Some (b, w)) -> List.filter (fun x -> let result2 = Code.reponse code x in (match  result2 with
				Some (b2, w2) -> b2 = b && w2 = w
				| None -> false)) listPossible
			| _ -> listPossible)
		| _ -> listPossible;;
	end;;
