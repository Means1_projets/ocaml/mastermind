RESULT     = mastermind
SOURCES    = Code.ml SDLUtils.ml IA.ml mastermind.ml
LIBS       = bigarray sdl sdlloader sdlttf sdlmixer sdlgfx sdlloader str unix
INCDIRS    = +sdl

include OCamlMakefile
