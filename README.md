# mastermind

Projet réalisé en Ocaml

Le jeu est un mastermind, il est possible de totalement configurer le jeu grâce aux options

Plusieurs IA sont à disposition pour le mode Solo : 
- Random
- MinMax
- Naïve
- MaxMax (volontairement moins forte que MinMax)

![](img/prez4.png)
![](img/prez3.png)
![](img/prez2.png)
![](img/prez1.png)

