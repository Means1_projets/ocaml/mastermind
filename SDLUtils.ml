(** Module de definition d'un code dans le jeu Mastermind **)
open Sdlevent
open Sdlkey
open Sdlloader
open Sdlvideo
open Sdlgfx
open Code


let _ = Sdlttf.init ();;
module SDLUtils:
	sig
		type screent

		val screen : screent

		val create : int -> int -> unit

		val setBackGround : Sdlvideo.color -> unit

		val rect : int -> int -> int -> int -> Sdlvideo.color -> unit

		val update : unit -> unit

		val circle : int -> int -> int -> Sdlvideo.color -> unit

		val loadImage : string -> Sdlvideo.surface

		val drawSurface : Sdlvideo.surface -> int -> int -> unit

		val loadTtf : string -> int -> Sdlttf.font

		val writeText : Sdlttf.font -> int -> int -> string -> Sdlvideo.color -> unit

		val resize : int -> int -> unit

	end = struct
		type screent = {mutable screen : Sdlvideo.surface option};;
		let screen = {screen = None};;

    	let create a b = Sdl.init [`VIDEO];
    					screen.screen <- Some(Sdlvideo.set_video_mode a b []);;
    	
    	let apply a = match screen.screen with
    			None -> failwith "Screen null"
    			| Some(screen) -> a screen; ();;

    	let setBackGround color = apply (fun screen -> Sdlvideo.fill_rect screen (Sdlvideo.map_RGB screen color));;

    	let rect a b c d color = apply (fun screen -> Sdlvideo.fill_rect ~rect:(Sdlvideo.rect a b c d) screen (Sdlvideo.map_RGB screen color));;

    	let update () = apply (fun screen -> Sdlvideo.flip screen);;		

    	let circle a b c color = apply (fun screen -> Sdlgfx.filledCircleRGBA screen (Sdlvideo.rect a b 0 0) c color 255);;

    	let loadImage name = Sdlloader.load_image name;;

    	let drawSurface surface x y = apply (fun screen -> Sdlvideo.blit_surface ~dst_rect:(Sdlvideo.rect x y 0 0) ~src:surface ~dst:screen ());;

    	let loadTtf name s = Sdlttf.open_font name s;;

    	let writeText font x y text color = drawSurface (Sdlttf.render_text_blended font text ~fg:color) x y;;

		let resize x y = Sdl.quit (); create x y;;

	end;;
