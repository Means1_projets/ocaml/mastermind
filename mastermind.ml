(* Chargement des modules que l'on utilise *)
open Sdlkey
open Sdlvideo
open Sdlevent
open Code
open SDLUtils
open Sdlmouse
open IA
open Unix
(* Type *)
type result = WIN | LOSE;;
type typeGame = PVP | PVE;;



(* Constante *)
let space = 10;;
let lg = 20;;
let lgp = 10;;
let spacep = 3;;
let spaceb = 50;;
let leftspace = 10;;
let rightspace = 10;;
let sizeButtonCheck = 67;;
let buttonbar = 100;;
let upspace = 10;;
let downspace = 0;;
let longbarre = 10;;



(* Variable - Initialisation - SDL *)
let button_check = SDLUtils.loadImage "check.png";;
let button_solo = SDLUtils.loadImage "solo.png";;
let button_multi = SDLUtils.loadImage "multi.png";;
let button_setting = SDLUtils.loadImage "settingIcon.png";;
let button_back = SDLUtils.loadImage "back.png";;
let font20 = SDLUtils.loadTtf "font.ttf" 20;;
let font32 = SDLUtils.loadTtf "font.ttf" 32;;
let font54 = SDLUtils.loadTtf "font.ttf" 54;;


(*Option *)
let manualResult = ref false;;
let manches = ref 2;;
let ia = ref 0;;
let nombreTry = ref 8;;
let nombreCode = ref Code.nombre_pions;;





(* Variable - Initialisation *)
let currentPos = ref ((!nombreTry)-1);;
let currentPion = ref None;;
let listPionPlaced = ref (Array.make_matrix (!nombreTry) ((!nombreCode)) None);;
let result = ref (Array.make (!nombreTry) (0,0));;
let typeCurrentGame = ref PVP;;
let maj = ref false;;
let playerName1 = ref "";;
let playerName2 = ref "";;
let nbx = ref ((((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar) / (space + lg*2));;
let nby = ref ((((List.length Code.couleurs_possibles) - 1)*(space + lg*2)) / (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar));;


(* Initialisation du random *)
Random.self_init ();;

let getLastTry () = List.fold_left (fun x y -> (List.map (fun x -> match x with Some(x) -> x | None -> failwith "Impossible") (Array.to_list y)) :: x) [] (Array.to_list (Array.sub !listPionPlaced 0 (!(nombreTry) - !(currentPos) - 1)));;


(** Renvoie le nom du joueur en fonction du nombre
		*	@param ID du joueur
		*	@return Le nom du joueur 
		*) 
let getName i = match i with 
				0 -> if (!playerName1) = "" then "Nesim" else (!playerName1)
				| _ -> if (!playerName2) = "" then "Jerome" else (!playerName2);;


(** Renvoie un code random
		*	@return Le code random 
		*)
let randomCode () = 
					let table = Array.make (!nombreCode) (List.nth Code.couleurs_possibles 0) in 
					for i = 0 to ((!nombreCode)-1) do 
						table.(i) <- List.nth Code.couleurs_possibles (Random.int (List.length Code.couleurs_possibles))
					done; 
	Array.to_list table;;




(** Affichage du menu  
		* 	@param int du joueur
		*	@return unit 
		*)
let printMenu ?edit:(i = -1) () = 
	SDLUtils.setBackGround (166, 124, 82);
	SDLUtils.rect 100 40 300 40 (if (i <> 0) then (162, 166, 151) else (10, 166, 151));
	SDLUtils.rect 100 80 300 40 (if (i <> 1) then (162, 166, 151) else (10, 166, 151));
	SDLUtils.rect 130 180 250 80 (162, 166, 151);
	SDLUtils.rect 130 280 250 80 (162, 166, 151);
	SDLUtils.rect 130 380 250 80 (162, 166, 151);
	SDLUtils.writeText font32 130 40 ("Joueur 1 : "^(getName 0)) Sdlvideo.blue;
	SDLUtils.writeText font32 130 80 ("Joueur 2 : "^(getName 1)) Sdlvideo.blue;
	SDLUtils.writeText font32 210 200 "Solo Player" Sdlvideo.blue;
	SDLUtils.writeText font32 210 300 "Multi Player" Sdlvideo.blue;
	SDLUtils.writeText font32 210 400 "Options" Sdlvideo.blue;
	SDLUtils.drawSurface button_solo 150 200;
	SDLUtils.drawSurface button_multi 150 300;
	SDLUtils.drawSurface button_setting 150 400;
	SDLUtils.update ();;

(** Affichage de l'interface de selection 
		* 	@param Pion' array Le code qu'il veut soumettre à l'adversaire 
		* 	@param int ID du joueur
		*	@return unit 
		*)
let printSelect arrayP joueur =
	SDLUtils.setBackGround (166, 124, 82);
	ignore (List.fold_left (fun x (Code.Pion(_,_,c)) -> let dy = (((x+1)*(space + lg*2)) / (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar)) in 
			SDLUtils.circle (leftspace + (x - (!nbx)*dy)*(space+lg*2) + lg) (upspace + longbarre + ((!nombreTry)+dy)*(space+lg*2) + lg) (lg) c;
			x + 1) 0 Code.couleurs_possibles);
	for i=0 to (!nombreCode)-1 do 
			match (arrayP.(i)) with
				Some(Code.Pion (_,_,c)) -> SDLUtils.circle (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + i*(space+lg*2) + lg) ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) (lg) c
				| None -> SDLUtils.circle (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + i*(space+lg*2) + lg) ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) (lg) (91, 51, 14)
	done;

	(match (!currentPion) with 
	Some(Code.Pion(_,_,c)) -> let (pos_x, pos_y, _) = Sdlmouse.get_state () in SDLUtils.circle pos_x pos_y (lg) c; ()
	| _ -> ());
	let count = ref 0 in
	for  j = 0 to ((!nombreCode))-1 do
		match (arrayP.(j)) with
		None -> ()
		| _ -> count := !count + 1	
	done;
	if ((!count) = ((!nombreCode))) then
		SDLUtils.drawSurface button_check (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace) (((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) - sizeButtonCheck/2)
	else ();
	SDLUtils.writeText font32 150 80 ("Le joueur "^(getName joueur)) Sdlvideo.blue;
	SDLUtils.writeText font32 80 120 "Doit choisir le code a trouver : " Sdlvideo.blue;
	SDLUtils.update ();;



(** Affichage de l'interface de selection du résultat  
		* 	@param Code tentative 
		* 	@param int ID du joueur
		*	@return unit 
		*)
let printResultTry result i =
	SDLUtils.setBackGround (166, 124, 82);
	SDLUtils.writeText font32 40 20 ("Joueur 1 : "^(getName i)) Sdlvideo.blue;
	SDLUtils.drawSurface button_check ((((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar)/2 - buttonbar/2) (((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) + lg*2 + space);
	SDLUtils.writeText font32 40 40 "Verifier la combinaison" Sdlvideo.blue;
	let tCode = (!listPionPlaced).((!nombreTry)-(!currentPos)-1) in
			begin
				for i=0 to ((!nombreCode))-1 do
					match (tCode.(i)) with
						Some(Code.Pion (_,_,c)) -> SDLUtils.circle (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + i*(space+lg*2) + lg) ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) (lg) c
						| None -> ()
				done;
				for j=0 to ((!nombreCode))-1 do
					SDLUtils.circle (leftspace + j*(spacep + lgp*2) + lgp) ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) (lgp) (match result.(j) with
						| Some(true) -> (0, 0, 0)
						| Some(false) -> (255, 255, 255)
						| None -> (91, 51, 14))
				done;
			end;
	SDLUtils.update ();;


(** Affichage de l'interface du jeu 
		* 	@param int Joueur qui joue 
		*	@return unit 
		*)
let printGame i = 
	SDLUtils.setBackGround (166, 124, 82);
	SDLUtils.writeText font32 (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre+8) 5 ("Joueur "^(string_of_int (i+1))) Sdlvideo.blue;
	SDLUtils.writeText font32 (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre+8) 30 (getName i) Sdlvideo.blue;
	if (i = 1 && !typeCurrentGame = PVE) then
		SDLUtils.writeText font32 (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre+8) 55 "(IA)" Sdlvideo.red;
	SDLUtils.rect 0 (upspace + (!currentPos)*(space+lg*2) - space/2) (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre/2) (space+lg*2) (196, 134, 78);
	SDLUtils.rect (leftspace + ((!nombreCode))*(spacep + lgp*2) + spacep/2 +spaceb/2 - longbarre/2) 0 (longbarre) (upspace + ((!nombreTry))*(space+lg*2)) (91, 51, 14);
	SDLUtils.rect (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre/2) 0 (longbarre) (upspace + ((!nombreTry))*(space+lg*2)) (91, 51, 14);
	SDLUtils.rect 0 (upspace + ((!nombreTry))*(space+lg*2) - space/2) (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar) longbarre (91, 51, 14);
	for i=0 to (!nombreTry)-1 do 
		let tCode = (!listPionPlaced).((!nombreTry)-i-1) and tResult = try Some((!result).((!nombreTry)-i-1)) with _ -> None in
			begin
				for j=0 to ((!nombreCode))-1 do
					match (tCode.(j)) with
						Some(Code.Pion (_,_,c)) -> SDLUtils.circle (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + j*(space+lg*2) + lg) (upspace + i*(space+lg*2) + lg) (lg) c
						| None -> SDLUtils.circle (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + j*(space+lg*2) + lg) (upspace + i*(space+lg*2) + lg) (lg) (91, 51, 14)
				done;
				match tResult with
				| Some(x, y) -> 	for j=0 to ((!nombreCode))-1 do
										if (j < x) then 
											SDLUtils.circle (leftspace + j*(spacep + lgp*2) + lgp) (upspace + i*(space+lg*2) + lg) (lgp) (0, 0, 0)
										else if (j < x + y) then
											SDLUtils.circle (leftspace + j*(spacep + lgp*2) + lgp) (upspace + i*(space+lg*2) + lg) (lgp) (255, 255, 255)
										else 
											SDLUtils.circle (leftspace + j*(spacep + lgp*2) + lgp) (upspace + i*(space+lg*2) + lg) (lgp) (91, 51, 14)
									done;
				| _ -> 		for j=0 to ((!nombreCode))-1 do
								SDLUtils.circle (leftspace + j*(spacep + lgp*2) + lgp) (upspace + i*(space+lg*2) + lg) (lgp) (91, 51, 14);
							done;
			end
	done;
	ignore (List.fold_left (fun x (Code.Pion(_,_,c)) -> let dy = (((x+1)*(space + lg*2)) / (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar)) in 
			SDLUtils.circle (leftspace + (x - (!nbx)*dy)*(space+lg*2) + lg) (upspace + longbarre + ((!nombreTry)+dy)*(space+lg*2) + lg) (lg) c;
			x + 1) 0 Code.couleurs_possibles);
	let count = ref 0 in
	let listP = (!listPionPlaced).((!nombreTry)-(!currentPos)-1) in
	for  j = 0 to ((!nombreCode))-1 do
		match (listP.(j)) with
		None -> ()
		| _ -> count := !count + 1	
	done;
	(match (!currentPion) with 
	Some(Code.Pion(_,_,c)) -> let (pos_x, pos_y, _) = Sdlmouse.get_state () in SDLUtils.circle pos_x pos_y (lg) c; ()
	| _ -> ());
	if ((!count) = (!nombreCode) && (i = 0 && !typeCurrentGame = PVE || !typeCurrentGame = PVP)) then
		SDLUtils.drawSurface button_check (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre) (upspace + ((!nombreTry))*(space+lg*2) - space / 2 - sizeButtonCheck)
	else ();
	SDLUtils.update ();;

(** Affichage de l'interface de fin du jeu (score) 
		* 	@param int Score du J1 
		* 	@param int Score du J2
		*	@return unit 
		*)
let ecranscore score1 score2 = 
	SDLUtils.setBackGround (166, 124, 82);
	SDLUtils.drawSurface button_back 0 0;
	if score1 > score2 then
		begin
			SDLUtils.writeText font54 100 40 "Le gagnant est : " Sdlvideo.blue;
			SDLUtils.writeText font54 180 100 (getName 0) Sdlvideo.blue;
			SDLUtils.writeText font32 200 160 ("avec " ^ (string_of_int score1) ^ " points") Sdlvideo.blue;
			SDLUtils.writeText font32 200 200 ("Perdant : " ^ " " ^ (getName 1)) Sdlvideo.blue;
			SDLUtils.writeText font32 200 240 ("avec " ^ (string_of_int score2) ^ " points") Sdlvideo.blue
		end
	else if score2 > score1 then
		begin
			SDLUtils.writeText font54 100 40 "Le gagnant est : " Sdlvideo.blue;
			SDLUtils.writeText font54 180 100 (getName 1) Sdlvideo.blue;
			SDLUtils.writeText font32 200 160 ("avec " ^ (string_of_int score2) ^ " points") Sdlvideo.blue;
			SDLUtils.writeText font32 200 200 ("Perdant : " ^ " " ^ (getName 0)) Sdlvideo.blue;
			SDLUtils.writeText font32 200 240 ("avec " ^ (string_of_int score1) ^ " points") Sdlvideo.blue
		end
	else begin
			SDLUtils.writeText font54 200 100 "Egalite !" Sdlvideo.blue;
			SDLUtils.writeText font32 200 160 ("avec " ^ (string_of_int score1) ^ " points") Sdlvideo.blue;
		end;
	SDLUtils.update ();;

(** Affichage de l'interface des options
		*	@return unit 
		*)
let printOption () = 
	SDLUtils.setBackGround (166, 124, 82);
	SDLUtils.drawSurface button_back 0 0;

	SDLUtils.writeText font32 120 40 "Nombre d'essaies :" (91, 51, 14);
	SDLUtils.writeText font32 120 130 "Nombre de pions :" (91, 51, 14);
	SDLUtils.writeText font32 120 220 "Nombres de manches : " (91, 51, 14);
	SDLUtils.writeText font32 120 310 "Choix de L'IA : " (91, 51, 14);
	SDLUtils.writeText font32 120 400 "Resultat Manuel : " (91, 51, 14);

	SDLUtils.writeText font32 320 40 (string_of_int (!nombreTry)) Sdlvideo.blue;
	SDLUtils.writeText font32 310 130 (string_of_int (!nombreCode)) Sdlvideo.blue;
	SDLUtils.writeText font32 360 220 (string_of_int (!manches)) Sdlvideo.blue;
	SDLUtils.writeText font32 285 310 (
		match !ia with
		| 0 -> "Random"
		| 1 -> "Naive"
		| 2 -> "Knuth - MaxMax"
		| 3 -> "Knuth"
		| _ -> "?") Sdlvideo.blue;
	SDLUtils.writeText font32 320 400 (match !manualResult with
	| true -> "Oui"
	| _ -> "Non") Sdlvideo.blue;
	SDLUtils.update ();;

























					
(** Gestion de l'affichage de selection du code 
		*	@return Code
		*)
let selectGestion i = 
		let arrayP = Array.make ((!nombreCode)) None and
 		running = ref true in printSelect arrayP i;
		while !running do
		    match wait_event () with
		    | QUIT ->
		        exit 0
		    | MOUSEMOTION {mme_x=pos_x ; mme_y=pos_y} -> 
				printSelect arrayP i
		    | MOUSEBUTTONDOWN {mbe_button = BUTTON_RIGHT ; mbe_x=pos_x ; mbe_y=pos_y} -> 
		    	begin 
		    	let decaly = (upspace + longbarre + ((!nombreTry))*(space+lg*2)) in if pos_x >= leftspace && pos_y >= decaly then 
			    		let x = (pos_x-leftspace)/(space+lg*2) and y = (pos_y-decaly)/(space+lg*2) in
			    			if x < (!nbx) && (x+y*(!nbx)) < List.length Code.couleurs_possibles then
				    			let x2 = (leftspace + (x)*(space+lg*2) + lg) and
								y2 = (upspace + longbarre + ((!nombreTry)+y)*(space+lg*2) + lg) in
			    				if (sqrt (float_of_int ((x2 - pos_x)*(x2 - pos_x) + (y2 - pos_y)*(y2 - pos_y)))) < float_of_int (lg) then
									begin
										let j = ref 0 in
										while (!j < (!nombreCode)) do
											(match arrayP.(!j) with
												None -> arrayP.(!j) <- Some(List.nth Code.couleurs_possibles (x+y*(!nbx)));
														j := (!nombreCode)
												| _ -> j := !j + 1
											)
										done;
								    	printSelect arrayP i
									end
								else ()
			    			else () 
					else ();
				let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
					y = ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2)-lg and
					w = (((!nombreCode))*(space+lg*2)) and
					h = lg*2 in
			    	begin 
			    		if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
				    		begin
				    			let cx = (pos_x-x)/(space + lg*2) 
				    			and dy = y + lg in
				    			let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
			    				if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
				   					arrayP.(cx) <- None;
								    printSelect arrayP i
				    		end
			    		else ()	
			      	end;
			end
			| MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x=pos_x ; mbe_y=pos_y} -> 
		    	begin 
		    	let x = (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace) and
		    	 	y = (((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) - sizeButtonCheck/2) in
				    if pos_x >= x && pos_y >= y && pos_x <= x + buttonbar && pos_y <= y + sizeButtonCheck then 
					 begin
					 	running := false
					 end else ();
				let decaly = (upspace + longbarre + ((!nombreTry))*(space+lg*2)) in if pos_x >= leftspace && pos_y >= decaly then 
			    		let x = (pos_x-leftspace)/(space+lg*2) and y = (pos_y-decaly)/(space+lg*2) in
			    			if x < (!nbx) && (x+y*(!nbx)) < List.length Code.couleurs_possibles then
				    			let x2 = (leftspace + (x)*(space+lg*2) + lg) and
								y2 = (upspace + longbarre + ((!nombreTry)+y)*(space+lg*2) + lg) in
			    				if (sqrt (float_of_int ((x2 - pos_x)*(x2 - pos_x) + (y2 - pos_y)*(y2 - pos_y)))) < float_of_int (lg) then
									begin
										currentPion := Some(List.nth Code.couleurs_possibles (x+y*(!nbx)));
								    	printSelect arrayP i
									end
								else ()
			    			else () 
					else ();
				let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
					y = ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2)-lg and
					w = (((!nombreCode))*(space+lg*2)) and
					h = lg*2 in
			    	begin 
			    		if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
				    		begin
				    			let cx = (pos_x-x)/(space + lg*2) 
				    			and dy = y + lg in
				    			let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
			    				if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
									currentPion := arrayP.(cx);
									arrayP.(cx) <- None;
									printSelect arrayP i 
				    		end
			    		else ()	
			      	end;
			end
			| MOUSEBUTTONUP {mbe_button = BUTTON_LEFT ; mbe_x=pos_x ; mbe_y=pos_y} ->
		    	(match (!currentPion) with 
		    		Some(_) -> 
								let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
									y = ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2)-lg and
									w = (((!nombreCode))*(space+lg*2)) and
									h = lg*2 in
							    	begin 
							    		if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
								    		begin
								    			let cx = (pos_x-x)/(space + lg*2) 
								    			and dy = y + lg in
								    			let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
								    				if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
								    					arrayP.(cx) <- (!currentPion)								    			else
													()
								    		end
							    		else ()
							    	end;
							    	currentPion := None;
									printSelect arrayP i
					| None -> ())
		    
			| _ -> ();
		done;
		List.map (fun x -> match x with | Some(x) -> x | None -> failwith "Error") (Array.to_list arrayP);;


(** Gestion de l'interface de selection du résultat 
		* 	@param int ID du joueur 
		*	@return (int * int) Résultat donné par l'adversaire 
		*)
let gestionResultTry i =
	let arrayR = Array.make ((!nombreCode)) None in
	printResultTry arrayR i;
	let running = ref true in 
		while !running do
		    match wait_event () with
		    | QUIT ->
		        exit 0
		    | MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x=pos_x ; mbe_y=pos_y} -> 
		    	begin 
					let y = ((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) and
					dx = ((!nombreCode) - 1)*(spacep + lgp*2) + lgp*2 in
						if (pos_x >= leftspace && pos_y >= y - lgp && pos_y <= y + lgp && pos_x <= leftspace + dx) then 
							begin 
								arrayR.((pos_x-leftspace)/(lgp*2+spacep)) <- (match arrayR.((pos_x-leftspace)/(lgp*2+spacep)) with
																			None -> Some(false)
																			| Some(false) -> Some (true)
																			| Some(true) -> None);
								printResultTry arrayR i
							end;
					let x = ((((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar)/2 - buttonbar/2) and
					    y = (((((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre)/2) + lg*2 + space) in
						if pos_x >= x && pos_y >= y && pos_x <= x + buttonbar && pos_y <= y + sizeButtonCheck then 
		    				running := false
				end 	
			| _ -> ();
		done;
		List.fold_left (fun (x1, x2) y -> match y with
									None -> (x1, x2)
									| Some(true) -> (x1+1, x2)
									| Some(false) -> (x1, x2+1)) (0,0) (Array.to_list arrayR);;



(** Gestion de l'interface de jeu 
		* 	@param int ID du joueur
		* 	@param int Code à trouver 
		*	@return result Retourne WIN ou LOSE 
		*)
let playerGame i code = 
	currentPos := ((!nombreTry)-1);
	currentPion := None;
	for i = 0 to (!nombreTry) - 1 do for j = 0 to ((!nombreCode)) - 1 do (!listPionPlaced).(i).(j) <- None done done;
	for i = 0 to (!nombreTry) - 1 do (!result).(i) <- (0,0) done;
	printGame i;

	let output = ref LOSE in
		if (!typeCurrentGame = PVP || i = 0) then 
		let running = ref true in 
			while !running do
			    match wait_event () with
			    | QUIT ->
			        exit 0
			    | MOUSEMOTION {mme_x=pos_x ; mme_y=pos_y} -> 
			    	printGame i
			    | MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x=pos_x ; mbe_y=pos_y} -> 
			    	begin 
					let x = (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre) and
					    y = (upspace + ((!nombreTry))*(space+lg*2) - space / 2 - sizeButtonCheck) in
						if pos_x >= x && pos_y >= y && pos_x <= x + buttonbar && pos_y <= y + sizeButtonCheck then 
						 begin
							let count = ref 0 in
							let listP = (!listPionPlaced).((!nombreTry)-(!currentPos)-1) in
								for  j = 0 to ((!nombreCode))-1 do
									match (listP.(j)) with
									None -> ()
									| _ -> count := !count + 1	
								done;
							if ((!count) = ((!nombreCode))) then
								let propose = List.map (fun x -> match x with | Some(x) -> x | None -> failwith "Error") (Array.to_list (!listPionPlaced).((!nombreTry) - !currentPos - 1)) in
			    	let result2 = Code.reponse propose code in (match result2 with
			    		Some(x,y) ->
			    					if (!manualResult) && ((!typeCurrentGame = PVE && i = 1) || (!typeCurrentGame = PVP)) then
			    						(match gestionResultTry ((i + 1) mod 2) with
			    						| (x2,y2) when x = x2 && y = y2 -> ()
			    						| _ -> output := WIN;
			    								running := false);

			    					(!result).((!nombreTry) - !currentPos - 1) <- (x,y);
			    					currentPos := !currentPos - 1;
			    					if ((!currentPos) = -1 || x = ((!nombreCode))) then 
			    						begin
				        					running := false;
				        					if (x = ((!nombreCode))) then 
				        						output := WIN;
			        					end
			        				else 
			        					printGame i
			    		| _ -> ()); else ();
						 end else ();	
					let decaly = (upspace + longbarre + ((!nombreTry))*(space+lg*2)) in if pos_x >= leftspace && pos_y >= decaly then 
				    		let x = (pos_x-leftspace)/(space+lg*2) and y = (pos_y-decaly)/(space+lg*2) in
				    			if x < (!nbx) && (x+y*(!nbx)) < List.length Code.couleurs_possibles then
					    			let x2 = (leftspace + (x)*(space+lg*2) + lg) and
									y2 = (upspace + longbarre + ((!nombreTry)+y)*(space+lg*2) + lg) in
				    				if (sqrt (float_of_int ((x2 - pos_x)*(x2 - pos_x) + (y2 - pos_y)*(y2 - pos_y)))) < float_of_int (lg) then
										begin
											currentPion := Some(List.nth Code.couleurs_possibles (x+y*(!nbx)));
									    	printGame i
										end
									else ()
				    			else () 
						else ();
					let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
						y = (upspace + (!currentPos)*(space+lg*2)) and
						w = (((!nombreCode))*(space+lg*2)) and
						h = lg*2 in
						begin 
							if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
								begin
									let cx = (pos_x-x)/(space + lg*2) 
									and dy = (upspace + (!currentPos)*(space+lg*2) + lg) in
									let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
									if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
										begin 
											currentPion := (!listPionPlaced).((!nombreTry) - !currentPos - 1).(cx);
											(!listPionPlaced).((!nombreTry) - !currentPos - 1).(cx) <- None;
											printGame i
										end
									else ()
								end
							else ()
						end;
				end
			    | MOUSEBUTTONDOWN {mbe_button = BUTTON_RIGHT ; mbe_x=pos_x ; mbe_y=pos_y} -> 
			    	begin 
			    	let decaly = (upspace + longbarre + ((!nombreTry))*(space+lg*2)) in if pos_x >= leftspace && pos_y >= decaly then 
				    		let x = (pos_x-leftspace)/(space+lg*2) and y = (pos_y-decaly)/(space+lg*2) in
				    			if x < (!nbx) && (x+y*(!nbx)) < List.length Code.couleurs_possibles then
					    			let x2 = (leftspace + (x)*(space+lg*2) + lg) and
									y2 = (upspace + longbarre + ((!nombreTry)+y)*(space+lg*2) + lg) in
				    				if (sqrt (float_of_int ((x2 - pos_x)*(x2 - pos_x) + (y2 - pos_y)*(y2 - pos_y)))) < float_of_int (lg) then
										begin
											let j = ref 0 in
											while (!j < (!nombreCode)) do
												(match (!listPionPlaced).((!nombreTry) - !currentPos - 1).(!j) with
													None -> (!listPionPlaced).((!nombreTry) - !currentPos - 1).(!j) <- Some(List.nth Code.couleurs_possibles (x+y*(!nbx)));
															j := (!nombreCode)
													| _ -> j := !j + 1
												)
											done;
									    	printGame i
										end
									else ()
				    			else () 
						else ();
					let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
						y = (upspace + (!currentPos)*(space+lg*2)) and
						w = (((!nombreCode))*(space+lg*2)) and
						h = lg*2 in
						begin 
							if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
								begin
									let cx = (pos_x-x)/(space + lg*2) 
									and dy = (upspace + (!currentPos)*(space+lg*2) + lg) in
									let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
									if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
										begin 
											(!listPionPlaced).((!nombreTry) - !currentPos - 1).(cx) <- None;
											printGame i
										end
									else ()
								end
							else ()
						end;
				end
			    | MOUSEBUTTONUP {mbe_button = BUTTON_LEFT ; mbe_x=pos_x ; mbe_y=pos_y} ->
			    	(match (!currentPion) with 
			    		Some(_) -> let x = ((!nombreCode))*(spacep+lgp*2) + spaceb + leftspace and
								    	y = (upspace + (!currentPos)*(space+lg*2)) and
								    	w = (((!nombreCode))*(space+lg*2)) and
								    	h = lg*2 in
								    	begin 
								    		if (pos_y > y && pos_y < y + h && pos_x > x && pos_x < x + w) then 
									    		begin
									    			let cx = (pos_x-x)/(space + lg*2) 
									    			and dy = (upspace + (!currentPos)*(space+lg*2) + lg) in
									    			let dx = (leftspace + ((!nombreCode))*(spacep + lgp*2) + spaceb + (cx)*(space+lg*2) + lg) in
									    				if (cx < ((!nombreCode)) && sqrt (float_of_int ((dx - pos_x)*(dx - pos_x) + (dy - pos_y)*(dy - pos_y))) < float_of_int (lg)) then
									    					(!listPionPlaced).((!nombreTry) - !currentPos - 1).(cx) <- (!currentPion)								    			else
														()
									    		end
								    		else ()
								    	end;
								    	currentPion := None;
								    	printGame i
						| None -> ())
			    | event ->
			        ();

		    done;	
		else 
			begin
				let possibleCode = ref (!Code.tous) in
				while (!currentPos > -1) do
					SDLUtils.writeText font20 (leftspace + ((!nombreCode))*(spacep + lgp*2 + space + lg*2) + spacep/2 + spaceb + longbarre+8) 90 "Je reflechis..." Sdlvideo.green;
					SDLUtils.update ();
					let prop = IA.choix (!ia) (getLastTry ()) (!possibleCode) (!nombreCode) in
						for i = 0 to (!nombreCode) - 1 do 
							(!listPionPlaced).((!nombreTry) - !currentPos - 1).(i) <- Some(List.nth prop i)
						done;
						let reponse = Code.reponse code prop in
							begin
								(match reponse with
			    				Some(x,y) ->
			    					if (!manualResult) && i = 1 then
			    						(match gestionResultTry (0) with
			    						| (x2,y2) when x = x2 && y = y2 -> ()
			    						| _ -> output := WIN;
			    								currentPos := -1);
			    					(!result).((!nombreTry) - !currentPos - 1) <- (x,y);
			        				printGame i;
			    					currentPos := !currentPos - 1;
			    					if ((!currentPos) = -1 || x = ((!nombreCode))) then 
			    						begin
			    								currentPos := -1;
				        					if (x = ((!nombreCode))) then 
				        						output := WIN;
			        					end
			        				else 
			        					possibleCode := (IA.filtre (!ia) (prop, reponse) (!possibleCode))
			    		| _ -> ())
							end;
					Unix.sleep 1
				done;
				Unix.sleep 2
			end;
		(!output);;


(** Gestion de l'edition des noms des joueurs
		* 	@param int ID du joueur
		*	@return unit 
		*)
let rec editName i =
	let running = ref true in
		let text = (if i = 0 then playerName1 else playerName2) in
		while !running do
			printMenu ~edit:i ();
			match wait_event () with
			    | KEYDOWN {keysym=KEY_LCTRL} -> 
					maj := not (!maj);
			    | KEYDOWN {keysym=KEY_SPACE} when String.length (!text) < 10 -> 
					text := (!text) ^ " "
				| KEYDOWN {keysym=KEY_ESCAPE} -> 
					running := false
		     	| KEYDOWN {keysym=KEY_BACKSPACE} -> 
					if (!text) = "" then () else
					text := (String.sub (!text) 0 ((String.length (!text)) - 1))
				| KEYDOWN {keysym=value} when String.length (!text) < 10 -> let valu = Sdlkey.int_of_key value in
			    		if (valu >= 97 && valu <= 122) then 
			    			text := (!text) ^ (let a = (String.make 1 (Sdlkey.char_of_key value)) in if (!maj) then String.uppercase a else a)
				| QUIT ->
					exit 0
				| MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x = pos_x  ; mbe_y = pos_y } -> 
					if pos_x >= 100 && pos_y >= 40 && pos_x <= 400 && pos_y <= 80 then
						editName 0
					else if pos_x >= 100 && pos_y >= 80 && pos_x <= 400 && pos_y <= 120 then
						editName 1;
					running := false
		    	| _ -> ();
			done; ();;




(** Gestion de l'interface de fin de jeu (score)
		* 	@param int score du joueur 1
		* 	@param int score du joueur 1
		*	@return unit 
		*)
let gestionEcranScore score1 score2 =
		let running = ref true in 
			ecranscore score1 score2;
			while !running do
			    match wait_event () with
			    | KEYDOWN {keysym=_} ->
					running := false
				| MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x = pos_x  ; mbe_y = pos_y } -> 
					if (pos_x <= 48 && pos_y <= 48) then
						running := false
		    	| QUIT -> exit 0
		    	| _ -> ()
			
			done;;


(** Gestion du pve
		* @return unit 
		*)
let pveGestion () = SDLUtils.resize (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar) (((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre);
					Code.tous_update (!nombreCode);
					Code.toutes_reponses_update (!nombreCode);
					typeCurrentGame := PVE;
					let scorePlayer1 = ref 0 and scorePlayer2 = ref 0 in
					let joueur = ref (Random.int 2) in
					for i = 0 to (!manches)-1 do 
						let code = (if (!joueur) = 1 then selectGestion 0 else randomCode ()) in
					    let result = playerGame (!joueur) (code) in 
						begin
							(match result with 
				   				WIN -> (match (!joueur) with
					   				| 0 -> scorePlayer1 := !scorePlayer1 + 1
					   				| _ -> scorePlayer2 := !scorePlayer2 + 1)
				   				| LOSE -> (match (!joueur) with
					   				| 1 -> scorePlayer1 := !scorePlayer1 + 1
					   				| _ -> scorePlayer2 := !scorePlayer2 + 1));
							joueur := (!joueur + 1) mod 2
						end
						done;					
					SDLUtils.resize 500 500;
					gestionEcranScore (!scorePlayer1) (!scorePlayer2);;




(** Gestion du pvp
		* @return unit 
		*)
let pvpGestion () = 
					SDLUtils.resize (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar) (((!nombreTry)+1+(!nby))*(space+lg*2) + upspace + downspace + longbarre);
					typeCurrentGame := PVP;
					let scorePlayer1 = ref 0 and scorePlayer2 = ref 0 in
					let joueur = ref (Random.int 2) in
					for i = 0 to (!manches)-1 do 
						let code = selectGestion (((!joueur) + 1) mod 2) in
					    let result = playerGame (!joueur) (code) in 
						begin
							(match result with 
				   				WIN -> (match (!joueur) with
					   				| 0 -> scorePlayer1 := !scorePlayer1 + 1
					   				| _ -> scorePlayer2 := !scorePlayer2 + 1)
				   				| LOSE -> (match (!joueur) with
					   				| 1 -> scorePlayer1 := !scorePlayer1 + 1
					   				| _ -> scorePlayer2 := !scorePlayer2 + 1));
							joueur := (!joueur + 1) mod 2
						end
					done;
					SDLUtils.resize 500 500;
					gestionEcranScore (!scorePlayer1) (!scorePlayer2);;



(** Gestion de l'interface des options
		*	@return unit 
		*)
let optionGestion () = 
	let running = ref true in 
			printOption ();
			while !running do
			    match wait_event () with
			    | KEYDOWN {keysym=KEY_ESCAPE} ->
					running := false
		    	| MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x = pos_x  ; mbe_y = pos_y } -> 
		    		if (pos_y <= 48 && pos_y <= 48) then
		    			running := false
					else begin
						if (pos_y <= 105) then nombreTry := (((!nombreTry) mod 15)+ 1)
						else if (pos_y <= 195) then nombreCode := (((!nombreCode) mod 10) + 1)
						else if (pos_y <= 285) then manches := ((!manches) mod 100) + 2
						else if (pos_y <= 375) then 
							ia := (((!ia) + 1) mod (IA.nombre_methodes))
						else if (pos_y <= 465) then
							manualResult := not (!manualResult);

						listPionPlaced := (Array.make_matrix (!nombreTry) ((!nombreCode)) None);
						result := (Array.make (!nombreTry) (0,0));
						printOption ()
					end
				| MOUSEBUTTONDOWN {mbe_button = BUTTON_RIGHT ; mbe_x = pos_x  ; mbe_y = pos_y } -> 
					if (pos_y <= 105) then 
						begin
							nombreTry := ((!nombreTry) - 1); 
							if (!nombreTry) = 0 then nombreTry := 15
						end
					else if (pos_y <= 195) then  
						begin
							nombreCode := (!nombreCode) -1; 
							if (!nombreCode) = 0 then nombreCode := 10
						end
					else if (pos_y <= 285) then  
						begin
							manches := (!manches) -2; 
							if (!manches) = 0 then manches := 100
						end
					else if (pos_y <= 375) then 
						begin
							ia := ((!ia) - 1);
							if (!ia) < 0 then ia := (IA.nombre_methodes - 1)
						end
					else if (pos_y <= 465) then
						manualResult := not (!manualResult);

					listPionPlaced := (Array.make_matrix (!nombreTry) ((!nombreCode)) None);
					result := (Array.make (!nombreTry) (0,0));
					nbx := ((((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar) / (space + lg*2));
					nby := ((((List.length Code.couleurs_possibles) - 1)*(space + lg*2)) / (((!nombreCode))*(spacep+lgp*2+space+lg*2) + spaceb + leftspace + rightspace + buttonbar));


					printOption ()
				| QUIT -> exit 0
		    	 | _ -> ()
			done;;


(** Gestion du menu
		* 	@return unit 
		*)
let main () =
	SDLUtils.create 500 500;
		let running = ref true in 
			while !running do
				printMenu ();
			    match wait_event () with
				    | MOUSEBUTTONDOWN {mbe_button = BUTTON_LEFT ; mbe_x = pos_x  ; mbe_y = pos_y } -> 
						if pos_x >= 130 && pos_y >= 180 && pos_x <= 380 && pos_y <= 260 then 
							pveGestion ()
						else if pos_x >= 130 && pos_y >= 280 && pos_x <= 380 && pos_y <= 360 then 
							pvpGestion ()
						else if pos_x >= 100 && pos_y >= 40 && pos_x <= 400 && pos_y <= 80 then
							editName 0
						else if pos_x >= 100 && pos_y >= 80 && pos_x <= 400 && pos_y <= 120 then
							editName 1
						else if pos_x >= 100 && pos_y >= 80 && pos_x <= 400 && pos_y <= 120 then
							editName 1
						else if pos_x >= 130 && pos_y >= 380 && pos_x <= 380 && pos_y <= 460 then
							optionGestion ()
					| KEYDOWN {keysym=KEY_ESCAPE} | QUIT ->
						running := false
			    	| _ -> ()
				
				done;;
		

let _ = main();;
